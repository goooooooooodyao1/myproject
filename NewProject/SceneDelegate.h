//
//  SceneDelegate.h
//  NewProject
//
//  Created by hoc on 2021/9/16.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

